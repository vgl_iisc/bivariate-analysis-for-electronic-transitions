# Bivariate analysis for electronic transitions #
This repository provides the code for performing bivariate analysis of electronic transitions using continuous scatterplot operators, specifically CSP peel and CSP lens. The implementation is based on the methodologies detailed in the following papers:

1. Segmentation Driven Peeling for Visual Analysis of Electronic Transitions.
	
	Mohit Sharma, Talha Bin Masood, Signe S. Thygesen, Mathieu Linares, Ingrid Hotz and Vijay Natarajan.
	
	IEEE VIS 2021, 96-100.
	
	https://doi.org/10.1109/VIS49827.2021.9623300
	
2. Continuous Scatterplot Operators for Bivariate Analysis and Study of Electronic Transitions
	
	Mohit Sharma, Talha Bin Masood, Signe S. Thygesen, Mathieu Linares, Ingrid Hotz and Vijay Natarajan.
	
	IEEE Transactions on Visualization and Computer Graphics, 2023.
	
	https://doi.org/10.1109/TVCG.2023.3237768


### Prerequisites ###
TTK 1.2 + Paraview 5.11.1 installed on Ubuntu 22.04 LTS.
Follow the steps on https://topology-tool-kit.github.io/installation-ubuntu-22.04.html to install the required TTK and Paraview versions.

## CSP Operators Paraview States##
* The Paraview state files for CSP peel and CSP lens operators are provided in this folder.
* The CSP peel operator takes the bivariate field and the segmentation information to compute the peeled CSPs corresponding to the individual segments.
* The CSP lens operator takes the CSP of the bivariate field as input and queries it directly.
* The state files require the path of a vti (bivariate field) file and a vtp (molecular structure) file to be provided.
* Both files are placed in the corresponding folders (CSP peel and CSP lens) for the current example.
* The state files (psvm) can be directly loaded in Paraview. Provide the path of the vti and vtp files in the prompt.
* For CSP peel operator to work, the subgroup information also needs to be provided. The state file provided in the "CSP peel" folder already has the information for Cu-PHE-PHE in the threshold filters Threshold_cu = 0-0, Threshold_PHE = 1-22, Threshold_rest = 23-44.
* Required: TTK 1.2 + Paraview 5.11.1. There may be errors when loading the files with any other version.

## data##
* The folder contains the remaining datasets used in the paper.
* Provide the appropriate vti, vtp file path while loading the the paraview state file to generate any other result.
* The threshold values for segmenting the various subgroups of different molecules are:
	Thiophene-Quinoxaline: Th = 0-7, Qu = 8-23
	Cu-PHE-PHE: Cu = 0-0, PHE = 1-22, PHE = 23-44
	Cu-PHE-PHEOME: Cu = 0-0, PHE = 1-22, PHE = 23-52
	Cu-PHE-XANT: Cu = 0-0, PHE = 1-22, PHE = 23-96
	
## Quantification##
* The folder contains the code to quntify the CSPs using discrete histogram based method. The usage is mentioned in the file header.
		
## Replicability Stamp ##
The folder contains the code and instructions to generate the individual images shown in Fig 11 for the replicability stamp.


## References
Please refer to the following papers for more detailed information about the method. If you use this method, code or data in your work, kindly cite these publications.

1. Segmentation Driven Peeling for Visual Analysis of Electronic Transitions.

	Mohit Sharma, Talha Bin Masood, Signe S. Thygesen, Mathieu Linares, Ingrid Hotz and Vijay Natarajan.

	IEEE VIS 2021, 96-100.

	https://doi.org/10.1109/VIS49827.2021.9623300
	
2. Continuous Scatterplot Operators for Bivariate Analysis and Study of Electronic Transitions
	
	Mohit Sharma, Talha Bin Masood, Signe S. Thygesen, Mathieu Linares, Ingrid Hotz and Vijay Natarajan.
	
	IEEE Transactions on Visualization and Computer Graphics, 2023.
	
	https://doi.org/10.1109/TVCG.2023.3237768

## Copyright
Copyright (c) 2021 Visualization & Graphics Lab (VGL), Indian Institute of Science. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
Authors   : Mohit Sharma, Talha Bin Masood

Contact  : mohitsharma@iisc.ac.in, talha.bin.masood@liu.se

## Acknowledgements
This work is supported by an Indo-Swedish joint network project: DST/INT/SWD/VR/P-02/2019.
