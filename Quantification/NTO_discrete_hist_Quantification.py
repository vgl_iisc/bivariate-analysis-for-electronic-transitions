# Usage python3 NTO_discrete_hist_Quantification.py <vti file name> -s <(histogram resolution -1) / 2> -i <subgroup threshold start indices>
# Example python3 NTO_discrete_hist_Quantification.py cu-phe-pheome-state1.vti -s 400 -i 0 1 23   #For an 801 X 801 discrete histogram

import numpy
import vtk 
from vtk.util.numpy_support import vtk_to_numpy
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("vti_file",
                        type = str,
                        help = "The .vti file path.")
    parser.add_argument("-s", "--hist_size", 
                        type = int,
                        default = 500,
                        help = "The grid size of the output discrete histogram.")
    parser.add_argument("-i", "--subgroup_start_indices", 
                        nargs = "+", 
                        type = int,
                        action='store', dest='subgroup_start_indices',
                        help = "List of start indices of the subgroups.")
    args = parser.parse_args()

    reader = vtk.vtkXMLImageDataReader()
    reader.SetFileName(args.vti_file)
    reader.Update()

    hole_nto = vtk_to_numpy(reader.GetOutput().GetPointData().GetArray("hole_nto"))
    particle_nto = vtk_to_numpy(reader.GetOutput().GetPointData().GetArray("particle_nto"))
    segment_ID = vtk_to_numpy(reader.GetOutput().GetPointData().GetArray("segment_ID"))

    min_hole_nto = numpy.min(hole_nto)
    max_hole_nto = numpy.max(hole_nto)
    min_particle_nto = numpy.min(particle_nto)
    max_particle_nto = numpy.max(particle_nto)

    MAX_ABS_NTO = max([abs(min_hole_nto), max_hole_nto, abs(min_particle_nto), max_particle_nto])
    HIST_SIZE = args.hist_size

    actual_hist_size = HIST_SIZE * 2 + 1
    hist = numpy.zeros((actual_hist_size, actual_hist_size), numpy.int32)

    bin_size = MAX_ABS_NTO / HIST_SIZE
    MIN_ABS_NTO = bin_size / 2

    subgroups_start_idx = []
    subgroups_end_idx = []
    subgroups_names = []
    max_index = numpy.max(segment_ID) + 1
    
    for i in range(1, len(args.subgroup_start_indices)):
        subgroups_start_idx.append(args.subgroup_start_indices[i - 1])
        subgroups_end_idx.append(args.subgroup_start_indices[i])
        subgroups_names.append("Subgroup %d" % i)

    subgroups_start_idx.append(args.subgroup_start_indices[-1])
    subgroups_end_idx.append(max_index)
    subgroups_names.append("Subgroup %d" % len(subgroups_start_idx))

    # Add the complete molecule as the last "subgroup"
    subgroups_start_idx.append(0)
    subgroups_end_idx.append(max_index)
    subgroups_names.append("Comp. molecule")

    # For each subgroup compute the discrete histogram and the charge transfer measure
    for sub_idx in range(len(subgroups_start_idx)): 
        # First construct the discrete histogram
        hist = numpy.zeros((actual_hist_size, actual_hist_size), numpy.int32)
        for i in range(len(hole_nto)):
            hole_value = hole_nto[i]
            particle_value = particle_nto[i]

            if segment_ID[i] < subgroups_start_idx[sub_idx] or segment_ID[i] >= subgroups_end_idx[sub_idx]:
                # Ignore the segments which are not part of this subgroup
                continue

            if abs(hole_value) < MIN_ABS_NTO:
                hole_idx = HIST_SIZE
            elif hole_value >= 0:
                hole_idx = int((hole_value - MIN_ABS_NTO) / bin_size)
                hole_idx = hole_idx + HIST_SIZE + 1
                if hole_idx >= actual_hist_size:
                    hole_idx = actual_hist_size - 1
            else:
                hole_idx = int((hole_value + MIN_ABS_NTO) / bin_size)
                hole_idx = hole_idx + HIST_SIZE - 1
                if hole_idx < 0:
                    hole_idx = 0

            if abs(particle_value) < MIN_ABS_NTO:
                particle_idx = HIST_SIZE
            elif particle_value >= 0:
                particle_idx = int((particle_value - MIN_ABS_NTO) / bin_size)
                particle_idx = particle_idx + HIST_SIZE + 1
                if particle_idx >= actual_hist_size:
                    particle_idx = actual_hist_size - 1
            else:
                particle_idx = int((particle_value + MIN_ABS_NTO) / bin_size)
                particle_idx = particle_idx + HIST_SIZE - 1
                if particle_idx < 0:
                    particle_idx = 0

            hist[hole_idx][particle_idx] = hist[hole_idx][particle_idx] + 1

        # Then compute the measure with an error estimate
        measure = 0.0
        error = 0.0
        for hole_idx in range(actual_hist_size):
            hole_value = 0.0
            if hole_idx < HIST_SIZE:
                hole_value = (hole_idx - HIST_SIZE + 0.5) * bin_size - MIN_ABS_NTO
            elif hole_idx > HIST_SIZE:
                hole_value = (hole_idx - HIST_SIZE - 0.5) * bin_size + MIN_ABS_NTO
            max_hole_error = 0.25 * bin_size * bin_size + abs(hole_value) * bin_size

            for particle_idx in range(actual_hist_size):
                if hist[hole_idx][particle_idx] < 1:
                    continue
                particle_value = 0.0
                if particle_idx < HIST_SIZE:
                    particle_value = (particle_idx - HIST_SIZE + 0.5) * bin_size - MIN_ABS_NTO
                elif particle_idx > HIST_SIZE:
                    particle_value = (particle_idx - HIST_SIZE - 0.5) * bin_size + MIN_ABS_NTO
                max_particle_error = 0.25 * bin_size * bin_size + abs(particle_value) * bin_size

                hist_val = hist[hole_idx][particle_idx]
                mid_measure = (hole_value * hole_value) - (particle_value * particle_value)
                measure = measure + hist_val * mid_measure

                error = error + hist_val * (max_hole_error + max_particle_error)
                
        print(f"{subgroups_names[sub_idx]} \t\t: {measure} +/- {error}")
