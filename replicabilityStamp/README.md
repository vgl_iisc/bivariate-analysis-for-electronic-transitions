## Replicability Stamp ##

### Steps to run ###
* Copy the "replicabilityStamp" folder locally
* Open a terminal and run the following commands
* cd replicabilityStamp/scripts 
* chmod 777 run.sh
* ./run.sh
* The individual images shown in Fig 11 of the paper can be found in replicabilityStamp/images
