# trace generated using paraview version 5.11.1
#import paraview
#paraview.compatibility.major = 5
#paraview.compatibility.minor = 11

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# create a new 'XML Image Data Reader'
cuphepheomestate10vti = XMLImageDataReader(registrationName='cu-phe-xant-state3.vti', FileName=['../data/cu-phe-xant-state3.vti'])
cuphepheomestate10vti.PointArrayStatus = ['hole_nto', 'particle_nto', 'hole_charge_density', 'particle_charge_density', 'segment_ID']

# Properties modified on cuphepheomestate10vti
cuphepheomestate10vti.TimeArray = 'None'

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')

# show data in view
cuphepheomestate10vtiDisplay = Show(cuphepheomestate10vti, renderView1, 'UniformGridRepresentation')

# trace defaults for the display properties.
cuphepheomestate10vtiDisplay.Representation = 'Outline'
cuphepheomestate10vtiDisplay.ColorArrayName = [None, '']
cuphepheomestate10vtiDisplay.SelectTCoordArray = 'None'
cuphepheomestate10vtiDisplay.SelectNormalArray = 'None'
cuphepheomestate10vtiDisplay.SelectTangentArray = 'None'
cuphepheomestate10vtiDisplay.OSPRayScaleArray = 'hole_charge_density'
cuphepheomestate10vtiDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
cuphepheomestate10vtiDisplay.SelectOrientationVectors = 'None'
cuphepheomestate10vtiDisplay.ScaleFactor = 1.739041571944
cuphepheomestate10vtiDisplay.SelectScaleArray = 'None'
cuphepheomestate10vtiDisplay.GlyphType = 'Arrow'
cuphepheomestate10vtiDisplay.GlyphTableIndexArray = 'None'
cuphepheomestate10vtiDisplay.GaussianRadius = 0.0869520785972
cuphepheomestate10vtiDisplay.SetScaleArray = ['POINTS', 'hole_charge_density']
cuphepheomestate10vtiDisplay.ScaleTransferFunction = 'PiecewiseFunction'
cuphepheomestate10vtiDisplay.OpacityArray = ['POINTS', 'hole_charge_density']
cuphepheomestate10vtiDisplay.OpacityTransferFunction = 'PiecewiseFunction'
cuphepheomestate10vtiDisplay.DataAxesGrid = 'GridAxesRepresentation'
cuphepheomestate10vtiDisplay.PolarAxes = 'PolarAxesRepresentation'
cuphepheomestate10vtiDisplay.ScalarOpacityUnitDistance = 0.3314954799473577
cuphepheomestate10vtiDisplay.OpacityArrayName = ['POINTS', 'hole_charge_density']
cuphepheomestate10vtiDisplay.ColorArray2Name = ['POINTS', 'hole_charge_density']
cuphepheomestate10vtiDisplay.SliceFunction = 'Plane'
cuphepheomestate10vtiDisplay.Slice = 46
cuphepheomestate10vtiDisplay.SelectInputVectors = [None, '']
cuphepheomestate10vtiDisplay.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'OSPRayScaleFunction'
cuphepheomestate10vtiDisplay.OSPRayScaleFunction.Points = [0.0, 0.0, 0.5, 0.0, 3.0, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
cuphepheomestate10vtiDisplay.ScaleTransferFunction.Points = [1.7719646593435017e-21, 0.0, 0.5, 0.0, 0.03119562193751335, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
cuphepheomestate10vtiDisplay.OpacityTransferFunction.Points = [1.7719646593435017e-21, 0.0, 0.5, 0.0, 0.03119562193751335, 1.0, 0.5, 0.0]

# init the 'Plane' selected for 'SliceFunction'
cuphepheomestate10vtiDisplay.SliceFunction.Origin = [6.899458410429999, 6.899458410429999, 8.69520785972]

# reset view to fit data
renderView1.ResetCamera(False)

# get the material library
materialLibrary1 = GetMaterialLibrary()

# update the view to ensure updated data information
renderView1.Update()

# hide data in view
Hide(cuphepheomestate10vti, renderView1)

# get layout
layout1 = GetLayout()

# split cell
layout1.SplitVertical(0, 0.5)

# set active view
SetActiveView(None)

# set active view
SetActiveView(renderView1)

# toggle interactive widget visibility (only when running from the GUI)
ShowInteractiveWidgets(proxy=cuphepheomestate10vtiDisplay.SliceFunction)

# toggle interactive widget visibility (only when running from the GUI)
ShowInteractiveWidgets(proxy=cuphepheomestate10vtiDisplay)

# toggle interactive widget visibility (only when running from the GUI)
HideInteractiveWidgets(proxy=cuphepheomestate10vtiDisplay.SliceFunction)

# toggle interactive widget visibility (only when running from the GUI)
HideInteractiveWidgets(proxy=cuphepheomestate10vtiDisplay)

# Properties modified on renderView1
renderView1.OrientationAxesVisibility = 0

# split cell
layout1.SplitHorizontal(1, 0.5)

# set active view
SetActiveView(None)

# set active view
SetActiveView(renderView1)

# toggle interactive widget visibility (only when running from the GUI)
ShowInteractiveWidgets(proxy=cuphepheomestate10vtiDisplay.SliceFunction)

# toggle interactive widget visibility (only when running from the GUI)
ShowInteractiveWidgets(proxy=cuphepheomestate10vtiDisplay)

# toggle interactive widget visibility (only when running from the GUI)
HideInteractiveWidgets(proxy=cuphepheomestate10vtiDisplay.SliceFunction)

# toggle interactive widget visibility (only when running from the GUI)
HideInteractiveWidgets(proxy=cuphepheomestate10vtiDisplay)

# split cell
layout1.SplitHorizontal(3, 0.5)

# set active view
SetActiveView(None)

# split cell
layout1.SplitHorizontal(4, 0.5)

# set active view
SetActiveView(renderView1)

# toggle interactive widget visibility (only when running from the GUI)
ShowInteractiveWidgets(proxy=cuphepheomestate10vtiDisplay.SliceFunction)

# toggle interactive widget visibility (only when running from the GUI)
ShowInteractiveWidgets(proxy=cuphepheomestate10vtiDisplay)

# toggle interactive widget visibility (only when running from the GUI)
HideInteractiveWidgets(proxy=cuphepheomestate10vtiDisplay.SliceFunction)

# toggle interactive widget visibility (only when running from the GUI)
HideInteractiveWidgets(proxy=cuphepheomestate10vtiDisplay)

# set active source
SetActiveSource(cuphepheomestate10vti)

# show data in view
cuphepheomestate10vtiDisplay = Show(cuphepheomestate10vti, renderView1, 'UniformGridRepresentation')

# reset view to fit data
renderView1.ResetCamera(False)

# set active source
SetActiveSource(cuphepheomestate10vti)

# create a new 'TTK ContinuousScatterPlot'
tTKContinuousScatterPlot1 = TTKContinuousScatterPlot(registrationName='TTKContinuousScatterPlot1', Input=cuphepheomestate10vti)
tTKContinuousScatterPlot1.ScalarField1 = ['POINTS', 'hole_charge_density']
tTKContinuousScatterPlot1.ScalarField2 = ['POINTS', 'hole_charge_density']

# Properties modified on tTKContinuousScatterPlot1
tTKContinuousScatterPlot1.ScalarField1 = ['POINTS', 'hole_nto']
tTKContinuousScatterPlot1.ScalarField2 = ['POINTS', 'particle_nto']

# show data in view
tTKContinuousScatterPlot1Display = Show(tTKContinuousScatterPlot1, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
tTKContinuousScatterPlot1Display.Representation = 'Surface'
tTKContinuousScatterPlot1Display.ColorArrayName = [None, '']
tTKContinuousScatterPlot1Display.SelectTCoordArray = 'None'
tTKContinuousScatterPlot1Display.SelectNormalArray = 'None'
tTKContinuousScatterPlot1Display.SelectTangentArray = 'None'
tTKContinuousScatterPlot1Display.OSPRayScaleArray = 'Density'
tTKContinuousScatterPlot1Display.OSPRayScaleFunction = 'PiecewiseFunction'
tTKContinuousScatterPlot1Display.SelectOrientationVectors = 'None'
tTKContinuousScatterPlot1Display.ScaleFactor = 0.03138069361448288
tTKContinuousScatterPlot1Display.SelectScaleArray = 'Density'
tTKContinuousScatterPlot1Display.GlyphType = 'Arrow'
tTKContinuousScatterPlot1Display.GlyphTableIndexArray = 'Density'
tTKContinuousScatterPlot1Display.GaussianRadius = 0.0015690346807241441
tTKContinuousScatterPlot1Display.SetScaleArray = ['POINTS', 'Density']
tTKContinuousScatterPlot1Display.ScaleTransferFunction = 'PiecewiseFunction'
tTKContinuousScatterPlot1Display.OpacityArray = ['POINTS', 'Density']
tTKContinuousScatterPlot1Display.OpacityTransferFunction = 'PiecewiseFunction'
tTKContinuousScatterPlot1Display.DataAxesGrid = 'GridAxesRepresentation'
tTKContinuousScatterPlot1Display.PolarAxes = 'PolarAxesRepresentation'
tTKContinuousScatterPlot1Display.ScalarOpacityUnitDistance = 0.0020231117538414937
tTKContinuousScatterPlot1Display.OpacityArrayName = ['POINTS', 'Density']
tTKContinuousScatterPlot1Display.SelectInputVectors = [None, '']
tTKContinuousScatterPlot1Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'OSPRayScaleFunction'
tTKContinuousScatterPlot1Display.OSPRayScaleFunction.Points = [0.0, 0.0, 0.5, 0.0, 3.0, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
tTKContinuousScatterPlot1Display.ScaleTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 302731.65623494633, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
tTKContinuousScatterPlot1Display.OpacityTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 302731.65623494633, 1.0, 0.5, 0.0]

# hide data in view
Hide(cuphepheomestate10vti, renderView1)

# update the view to ensure updated data information
renderView1.Update()

# set active source
SetActiveSource(cuphepheomestate10vti)

# toggle interactive widget visibility (only when running from the GUI)
ShowInteractiveWidgets(proxy=cuphepheomestate10vtiDisplay.SliceFunction)

# toggle interactive widget visibility (only when running from the GUI)
ShowInteractiveWidgets(proxy=cuphepheomestate10vtiDisplay)

# toggle interactive widget visibility (only when running from the GUI)
HideInteractiveWidgets(proxy=cuphepheomestate10vtiDisplay.SliceFunction)

# toggle interactive widget visibility (only when running from the GUI)
HideInteractiveWidgets(proxy=cuphepheomestate10vtiDisplay)

# show data in view
cuphepheomestate10vtiDisplay = Show(cuphepheomestate10vti, renderView1, 'UniformGridRepresentation')

# hide data in view
Hide(cuphepheomestate10vti, renderView1)

# set active source
SetActiveSource(cuphepheomestate10vti)

# create a new 'Threshold'
threshold1 = Threshold(registrationName='Threshold1', Input=cuphepheomestate10vti)
threshold1.Scalars = ['POINTS', 'hole_charge_density']
threshold1.LowerThreshold = 1.7719646593435017e-21
threshold1.UpperThreshold = 0.03119562193751335

# set active source
SetActiveSource(cuphepheomestate10vti)

# toggle interactive widget visibility (only when running from the GUI)
ShowInteractiveWidgets(proxy=cuphepheomestate10vtiDisplay.SliceFunction)

# toggle interactive widget visibility (only when running from the GUI)
ShowInteractiveWidgets(proxy=cuphepheomestate10vtiDisplay)

# toggle interactive widget visibility (only when running from the GUI)
HideInteractiveWidgets(proxy=cuphepheomestate10vtiDisplay.SliceFunction)

# toggle interactive widget visibility (only when running from the GUI)
HideInteractiveWidgets(proxy=cuphepheomestate10vtiDisplay)

# create a new 'Threshold'
threshold2 = Threshold(registrationName='Threshold2', Input=cuphepheomestate10vti)
threshold2.Scalars = ['POINTS', 'hole_charge_density']
threshold2.LowerThreshold = 1.7719646593435017e-21
threshold2.UpperThreshold = 0.03119562193751335

# set active source
SetActiveSource(cuphepheomestate10vti)

# toggle interactive widget visibility (only when running from the GUI)
ShowInteractiveWidgets(proxy=cuphepheomestate10vtiDisplay.SliceFunction)

# toggle interactive widget visibility (only when running from the GUI)
ShowInteractiveWidgets(proxy=cuphepheomestate10vtiDisplay)

# toggle interactive widget visibility (only when running from the GUI)
HideInteractiveWidgets(proxy=cuphepheomestate10vtiDisplay.SliceFunction)

# toggle interactive widget visibility (only when running from the GUI)
HideInteractiveWidgets(proxy=cuphepheomestate10vtiDisplay)

# create a new 'Threshold'
threshold3 = Threshold(registrationName='Threshold3', Input=cuphepheomestate10vti)
threshold3.Scalars = ['POINTS', 'hole_charge_density']
threshold3.LowerThreshold = 1.7719646593435017e-21
threshold3.UpperThreshold = 0.03119562193751335

# set active source
SetActiveSource(threshold1)

# rename source object
RenameSource('Threshold_cu', threshold1)

# set active source
SetActiveSource(threshold2)

# rename source object
RenameSource('Threshold_phe', threshold2)

# set active source
SetActiveSource(threshold3)

# rename source object
RenameSource('Threshold_rest', threshold3)

# set active source
SetActiveSource(threshold1)

# set active source
SetActiveSource(threshold2)

# set active source
SetActiveSource(threshold3)

# set active source
SetActiveSource(threshold2)

# set active source
SetActiveSource(threshold1)

# Properties modified on threshold2
threshold2.Scalars = ['POINTS', 'segment_ID']
threshold2.LowerThreshold = 1.0
threshold2.UpperThreshold = 22.0

# show data in view
threshold2Display = Show(threshold2, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
threshold2Display.Representation = 'Surface'
threshold2Display.ColorArrayName = [None, '']
threshold2Display.SelectTCoordArray = 'None'
threshold2Display.SelectNormalArray = 'None'
threshold2Display.SelectTangentArray = 'None'
threshold2Display.OSPRayScaleArray = 'hole_charge_density'
threshold2Display.OSPRayScaleFunction = 'PiecewiseFunction'
threshold2Display.SelectOrientationVectors = 'None'
threshold2Display.ScaleFactor = 1.3798916816711426
threshold2Display.SelectScaleArray = 'None'
threshold2Display.GlyphType = 'Arrow'
threshold2Display.GlyphTableIndexArray = 'None'
threshold2Display.GaussianRadius = 0.06899458408355713
threshold2Display.SetScaleArray = ['POINTS', 'hole_charge_density']
threshold2Display.ScaleTransferFunction = 'PiecewiseFunction'
threshold2Display.OpacityArray = ['POINTS', 'hole_charge_density']
threshold2Display.OpacityTransferFunction = 'PiecewiseFunction'
threshold2Display.DataAxesGrid = 'GridAxesRepresentation'
threshold2Display.PolarAxes = 'PolarAxesRepresentation'
threshold2Display.ScalarOpacityUnitDistance = 0.4100933348606373
threshold2Display.OpacityArrayName = ['POINTS', 'hole_charge_density']
threshold2Display.SelectInputVectors = [None, '']
threshold2Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'OSPRayScaleFunction'
threshold2Display.OSPRayScaleFunction.Points = [0.0, 0.0, 0.5, 0.0, 3.0, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
threshold2Display.ScaleTransferFunction.Points = [1.3386828500648942e-20, 0.0, 0.5, 0.0, 3.2945346902124584e-05, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
threshold2Display.OpacityTransferFunction.Points = [1.3386828500648942e-20, 0.0, 0.5, 0.0, 3.2945346902124584e-05, 1.0, 0.5, 0.0]

# Properties modified on threshold1
threshold1.Scalars = ['POINTS', 'segment_ID']
threshold1.LowerThreshold = 0.0
threshold1.UpperThreshold = 0.0

# show data in view
threshold1Display = Show(threshold1, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
threshold1Display.Representation = 'Surface'
threshold1Display.ColorArrayName = [None, '']
threshold1Display.SelectTCoordArray = 'None'
threshold1Display.SelectNormalArray = 'None'
threshold1Display.SelectTangentArray = 'None'
threshold1Display.OSPRayScaleArray = 'hole_charge_density'
threshold1Display.OSPRayScaleFunction = 'PiecewiseFunction'
threshold1Display.SelectOrientationVectors = 'None'
threshold1Display.ScaleFactor = 0.4347603797912598
threshold1Display.SelectScaleArray = 'None'
threshold1Display.GlyphType = 'Arrow'
threshold1Display.GlyphTableIndexArray = 'None'
threshold1Display.GaussianRadius = 0.02173801898956299
threshold1Display.SetScaleArray = ['POINTS', 'hole_charge_density']
threshold1Display.ScaleTransferFunction = 'PiecewiseFunction'
threshold1Display.OpacityArray = ['POINTS', 'hole_charge_density']
threshold1Display.OpacityTransferFunction = 'PiecewiseFunction'
threshold1Display.DataAxesGrid = 'GridAxesRepresentation'
threshold1Display.PolarAxes = 'PolarAxesRepresentation'
threshold1Display.ScalarOpacityUnitDistance = 0.4260978679132151
threshold1Display.OpacityArrayName = ['POINTS', 'hole_charge_density']
threshold1Display.SelectInputVectors = [None, '']
threshold1Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'OSPRayScaleFunction'
threshold1Display.OSPRayScaleFunction.Points = [0.0, 0.0, 0.5, 0.0, 3.0, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
threshold1Display.ScaleTransferFunction.Points = [1.3129354374530333e-11, 0.0, 0.5, 0.0, 0.03119562193751335, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
threshold1Display.OpacityTransferFunction.Points = [1.3129354374530333e-11, 0.0, 0.5, 0.0, 0.03119562193751335, 1.0, 0.5, 0.0]

# Properties modified on threshold3
threshold3.Scalars = ['POINTS', 'segment_ID']
threshold3.LowerThreshold = 23.0
threshold3.UpperThreshold = 96.0

# show data in view
threshold3Display = Show(threshold3, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
threshold3Display.Representation = 'Surface'
threshold3Display.ColorArrayName = [None, '']
threshold3Display.SelectTCoordArray = 'None'
threshold3Display.SelectNormalArray = 'None'
threshold3Display.SelectTangentArray = 'None'
threshold3Display.OSPRayScaleArray = 'hole_charge_density'
threshold3Display.OSPRayScaleFunction = 'PiecewiseFunction'
threshold3Display.SelectOrientationVectors = 'None'
threshold3Display.ScaleFactor = 1.6445283889770508
threshold3Display.SelectScaleArray = 'None'
threshold3Display.GlyphType = 'Arrow'
threshold3Display.GlyphTableIndexArray = 'None'
threshold3Display.GaussianRadius = 0.08222641944885255
threshold3Display.SetScaleArray = ['POINTS', 'hole_charge_density']
threshold3Display.ScaleTransferFunction = 'PiecewiseFunction'
threshold3Display.OpacityArray = ['POINTS', 'hole_charge_density']
threshold3Display.OpacityTransferFunction = 'PiecewiseFunction'
threshold3Display.DataAxesGrid = 'GridAxesRepresentation'
threshold3Display.PolarAxes = 'PolarAxesRepresentation'
threshold3Display.ScalarOpacityUnitDistance = 0.39321586493668315
threshold3Display.OpacityArrayName = ['POINTS', 'hole_charge_density']
threshold3Display.SelectInputVectors = [None, '']
threshold3Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'OSPRayScaleFunction'
threshold3Display.OSPRayScaleFunction.Points = [0.0, 0.0, 0.5, 0.0, 3.0, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
threshold3Display.ScaleTransferFunction.Points = [1.7719646593435017e-21, 0.0, 0.5, 0.0, 3.401023423066363e-05, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
threshold3Display.OpacityTransferFunction.Points = [1.7719646593435017e-21, 0.0, 0.5, 0.0, 3.401023423066363e-05, 1.0, 0.5, 0.0]

# update the view to ensure updated data information
renderView1.Update()

# set active source
SetActiveSource(tTKContinuousScatterPlot1)

# set active source
SetActiveSource(threshold1)

# create a new 'Tetrahedralize'
tetrahedralize1 = Tetrahedralize(registrationName='Tetrahedralize1', Input=threshold1)

# set active source
SetActiveSource(threshold2)

# create a new 'Tetrahedralize'
tetrahedralize2 = Tetrahedralize(registrationName='Tetrahedralize2', Input=threshold2)

# set active source
SetActiveSource(threshold3)

# create a new 'Tetrahedralize'
tetrahedralize3 = Tetrahedralize(registrationName='Tetrahedralize3', Input=threshold3)

# show data in view
tetrahedralize3Display = Show(tetrahedralize3, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
tetrahedralize3Display.Representation = 'Surface'
tetrahedralize3Display.ColorArrayName = [None, '']
tetrahedralize3Display.SelectTCoordArray = 'None'
tetrahedralize3Display.SelectNormalArray = 'None'
tetrahedralize3Display.SelectTangentArray = 'None'
tetrahedralize3Display.OSPRayScaleArray = 'hole_charge_density'
tetrahedralize3Display.OSPRayScaleFunction = 'PiecewiseFunction'
tetrahedralize3Display.SelectOrientationVectors = 'None'
tetrahedralize3Display.ScaleFactor = 1.6445283889770508
tetrahedralize3Display.SelectScaleArray = 'None'
tetrahedralize3Display.GlyphType = 'Arrow'
tetrahedralize3Display.GlyphTableIndexArray = 'None'
tetrahedralize3Display.GaussianRadius = 0.08222641944885255
tetrahedralize3Display.SetScaleArray = ['POINTS', 'hole_charge_density']
tetrahedralize3Display.ScaleTransferFunction = 'PiecewiseFunction'
tetrahedralize3Display.OpacityArray = ['POINTS', 'hole_charge_density']
tetrahedralize3Display.OpacityTransferFunction = 'PiecewiseFunction'
tetrahedralize3Display.DataAxesGrid = 'GridAxesRepresentation'
tetrahedralize3Display.PolarAxes = 'PolarAxesRepresentation'
tetrahedralize3Display.ScalarOpacityUnitDistance = 0.21639502985535047
tetrahedralize3Display.OpacityArrayName = ['POINTS', 'hole_charge_density']
tetrahedralize3Display.SelectInputVectors = [None, '']
tetrahedralize3Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'OSPRayScaleFunction'
tetrahedralize3Display.OSPRayScaleFunction.Points = [0.0, 0.0, 0.5, 0.0, 3.0, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
tetrahedralize3Display.ScaleTransferFunction.Points = [1.7719646593435017e-21, 0.0, 0.5, 0.0, 3.401023423066363e-05, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
tetrahedralize3Display.OpacityTransferFunction.Points = [1.7719646593435017e-21, 0.0, 0.5, 0.0, 3.401023423066363e-05, 1.0, 0.5, 0.0]

# hide data in view
Hide(threshold3, renderView1)

# show data in view
tetrahedralize1Display = Show(tetrahedralize1, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
tetrahedralize1Display.Representation = 'Surface'
tetrahedralize1Display.ColorArrayName = [None, '']
tetrahedralize1Display.SelectTCoordArray = 'None'
tetrahedralize1Display.SelectNormalArray = 'None'
tetrahedralize1Display.SelectTangentArray = 'None'
tetrahedralize1Display.OSPRayScaleArray = 'hole_charge_density'
tetrahedralize1Display.OSPRayScaleFunction = 'PiecewiseFunction'
tetrahedralize1Display.SelectOrientationVectors = 'None'
tetrahedralize1Display.ScaleFactor = 0.4347603797912598
tetrahedralize1Display.SelectScaleArray = 'None'
tetrahedralize1Display.GlyphType = 'Arrow'
tetrahedralize1Display.GlyphTableIndexArray = 'None'
tetrahedralize1Display.GaussianRadius = 0.02173801898956299
tetrahedralize1Display.SetScaleArray = ['POINTS', 'hole_charge_density']
tetrahedralize1Display.ScaleTransferFunction = 'PiecewiseFunction'
tetrahedralize1Display.OpacityArray = ['POINTS', 'hole_charge_density']
tetrahedralize1Display.OpacityTransferFunction = 'PiecewiseFunction'
tetrahedralize1Display.DataAxesGrid = 'GridAxesRepresentation'
tetrahedralize1Display.PolarAxes = 'PolarAxesRepresentation'
tetrahedralize1Display.ScalarOpacityUnitDistance = 0.234490693459758
tetrahedralize1Display.OpacityArrayName = ['POINTS', 'hole_charge_density']
tetrahedralize1Display.SelectInputVectors = [None, '']
tetrahedralize1Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'OSPRayScaleFunction'
tetrahedralize1Display.OSPRayScaleFunction.Points = [0.0, 0.0, 0.5, 0.0, 3.0, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
tetrahedralize1Display.ScaleTransferFunction.Points = [1.3129354374530333e-11, 0.0, 0.5, 0.0, 0.03119562193751335, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
tetrahedralize1Display.OpacityTransferFunction.Points = [1.3129354374530333e-11, 0.0, 0.5, 0.0, 0.03119562193751335, 1.0, 0.5, 0.0]

# hide data in view
Hide(threshold1, renderView1)

# show data in view
tetrahedralize2Display = Show(tetrahedralize2, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
tetrahedralize2Display.Representation = 'Surface'
tetrahedralize2Display.ColorArrayName = [None, '']
tetrahedralize2Display.SelectTCoordArray = 'None'
tetrahedralize2Display.SelectNormalArray = 'None'
tetrahedralize2Display.SelectTangentArray = 'None'
tetrahedralize2Display.OSPRayScaleArray = 'hole_charge_density'
tetrahedralize2Display.OSPRayScaleFunction = 'PiecewiseFunction'
tetrahedralize2Display.SelectOrientationVectors = 'None'
tetrahedralize2Display.ScaleFactor = 1.3798916816711426
tetrahedralize2Display.SelectScaleArray = 'None'
tetrahedralize2Display.GlyphType = 'Arrow'
tetrahedralize2Display.GlyphTableIndexArray = 'None'
tetrahedralize2Display.GaussianRadius = 0.06899458408355713
tetrahedralize2Display.SetScaleArray = ['POINTS', 'hole_charge_density']
tetrahedralize2Display.ScaleTransferFunction = 'PiecewiseFunction'
tetrahedralize2Display.OpacityArray = ['POINTS', 'hole_charge_density']
tetrahedralize2Display.OpacityTransferFunction = 'PiecewiseFunction'
tetrahedralize2Display.DataAxesGrid = 'GridAxesRepresentation'
tetrahedralize2Display.PolarAxes = 'PolarAxesRepresentation'
tetrahedralize2Display.ScalarOpacityUnitDistance = 0.22568305949440118
tetrahedralize2Display.OpacityArrayName = ['POINTS', 'hole_charge_density']
tetrahedralize2Display.SelectInputVectors = [None, '']
tetrahedralize2Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'OSPRayScaleFunction'
tetrahedralize2Display.OSPRayScaleFunction.Points = [0.0, 0.0, 0.5, 0.0, 3.0, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
tetrahedralize2Display.ScaleTransferFunction.Points = [1.3386828500648942e-20, 0.0, 0.5, 0.0, 3.2945346902124584e-05, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
tetrahedralize2Display.OpacityTransferFunction.Points = [1.3386828500648942e-20, 0.0, 0.5, 0.0, 3.2945346902124584e-05, 1.0, 0.5, 0.0]

# hide data in view
Hide(threshold2, renderView1)

# update the view to ensure updated data information
renderView1.Update()

# set active source
SetActiveSource(tetrahedralize1)

# create a new 'TTK ContinuousScatterPlot'
tTKContinuousScatterPlot2 = TTKContinuousScatterPlot(registrationName='TTKContinuousScatterPlot2', Input=tetrahedralize1)
tTKContinuousScatterPlot2.ScalarField1 = ['POINTS', 'hole_charge_density']
tTKContinuousScatterPlot2.ScalarField2 = ['POINTS', 'hole_charge_density']

# set active source
SetActiveSource(tetrahedralize2)

# create a new 'TTK ContinuousScatterPlot'
tTKContinuousScatterPlot3 = TTKContinuousScatterPlot(registrationName='TTKContinuousScatterPlot3', Input=tetrahedralize2)
tTKContinuousScatterPlot3.ScalarField1 = ['POINTS', 'hole_charge_density']
tTKContinuousScatterPlot3.ScalarField2 = ['POINTS', 'hole_charge_density']

# set active source
SetActiveSource(tetrahedralize3)

# create a new 'TTK ContinuousScatterPlot'
tTKContinuousScatterPlot4 = TTKContinuousScatterPlot(registrationName='TTKContinuousScatterPlot4', Input=tetrahedralize3)
tTKContinuousScatterPlot4.ScalarField1 = ['POINTS', 'hole_charge_density']
tTKContinuousScatterPlot4.ScalarField2 = ['POINTS', 'hole_charge_density']

# set active source
SetActiveSource(tTKContinuousScatterPlot3)

# set active source
SetActiveSource(tTKContinuousScatterPlot2)

# Properties modified on tTKContinuousScatterPlot2
tTKContinuousScatterPlot2.ScalarField1 = ['POINTS', 'hole_nto']
tTKContinuousScatterPlot2.ScalarField2 = ['POINTS', 'particle_nto']

# show data in view
tTKContinuousScatterPlot2Display = Show(tTKContinuousScatterPlot2, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
tTKContinuousScatterPlot2Display.Representation = 'Surface'
tTKContinuousScatterPlot2Display.ColorArrayName = [None, '']
tTKContinuousScatterPlot2Display.SelectTCoordArray = 'None'
tTKContinuousScatterPlot2Display.SelectNormalArray = 'None'
tTKContinuousScatterPlot2Display.SelectTangentArray = 'None'
tTKContinuousScatterPlot2Display.OSPRayScaleArray = 'Density'
tTKContinuousScatterPlot2Display.OSPRayScaleFunction = 'PiecewiseFunction'
tTKContinuousScatterPlot2Display.SelectOrientationVectors = 'None'
tTKContinuousScatterPlot2Display.ScaleFactor = 0.03138069361448288
tTKContinuousScatterPlot2Display.SelectScaleArray = 'Density'
tTKContinuousScatterPlot2Display.GlyphType = 'Arrow'
tTKContinuousScatterPlot2Display.GlyphTableIndexArray = 'Density'
tTKContinuousScatterPlot2Display.GaussianRadius = 0.0015690346807241441
tTKContinuousScatterPlot2Display.SetScaleArray = ['POINTS', 'Density']
tTKContinuousScatterPlot2Display.ScaleTransferFunction = 'PiecewiseFunction'
tTKContinuousScatterPlot2Display.OpacityArray = ['POINTS', 'Density']
tTKContinuousScatterPlot2Display.OpacityTransferFunction = 'PiecewiseFunction'
tTKContinuousScatterPlot2Display.DataAxesGrid = 'GridAxesRepresentation'
tTKContinuousScatterPlot2Display.PolarAxes = 'PolarAxesRepresentation'
tTKContinuousScatterPlot2Display.ScalarOpacityUnitDistance = 0.0019867926680049025
tTKContinuousScatterPlot2Display.OpacityArrayName = ['POINTS', 'Density']
tTKContinuousScatterPlot2Display.SelectInputVectors = [None, '']
tTKContinuousScatterPlot2Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'OSPRayScaleFunction'
tTKContinuousScatterPlot2Display.OSPRayScaleFunction.Points = [0.0, 0.0, 0.5, 0.0, 3.0, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
tTKContinuousScatterPlot2Display.ScaleTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 69104.44361641743, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
tTKContinuousScatterPlot2Display.OpacityTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 69104.44361641743, 1.0, 0.5, 0.0]

# hide data in view
Hide(tetrahedralize1, renderView1)

# Properties modified on tTKContinuousScatterPlot3
tTKContinuousScatterPlot3.ScalarField1 = ['POINTS', 'hole_nto']
tTKContinuousScatterPlot3.ScalarField2 = ['POINTS', 'particle_nto']

# show data in view
tTKContinuousScatterPlot3Display = Show(tTKContinuousScatterPlot3, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
tTKContinuousScatterPlot3Display.Representation = 'Surface'
tTKContinuousScatterPlot3Display.ColorArrayName = [None, '']
tTKContinuousScatterPlot3Display.SelectTCoordArray = 'None'
tTKContinuousScatterPlot3Display.SelectNormalArray = 'None'
tTKContinuousScatterPlot3Display.SelectTangentArray = 'None'
tTKContinuousScatterPlot3Display.OSPRayScaleArray = 'Density'
tTKContinuousScatterPlot3Display.OSPRayScaleFunction = 'PiecewiseFunction'
tTKContinuousScatterPlot3Display.SelectOrientationVectors = 'None'
tTKContinuousScatterPlot3Display.ScaleFactor = 0.00841129757463932
tTKContinuousScatterPlot3Display.SelectScaleArray = 'Density'
tTKContinuousScatterPlot3Display.GlyphType = 'Arrow'
tTKContinuousScatterPlot3Display.GlyphTableIndexArray = 'Density'
tTKContinuousScatterPlot3Display.GaussianRadius = 0.00042056487873196605
tTKContinuousScatterPlot3Display.SetScaleArray = ['POINTS', 'Density']
tTKContinuousScatterPlot3Display.ScaleTransferFunction = 'PiecewiseFunction'
tTKContinuousScatterPlot3Display.OpacityArray = ['POINTS', 'Density']
tTKContinuousScatterPlot3Display.OpacityTransferFunction = 'PiecewiseFunction'
tTKContinuousScatterPlot3Display.DataAxesGrid = 'GridAxesRepresentation'
tTKContinuousScatterPlot3Display.PolarAxes = 'PolarAxesRepresentation'
tTKContinuousScatterPlot3Display.ScalarOpacityUnitDistance = 0.0005281684956781287
tTKContinuousScatterPlot3Display.OpacityArrayName = ['POINTS', 'Density']
tTKContinuousScatterPlot3Display.SelectInputVectors = [None, '']
tTKContinuousScatterPlot3Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'OSPRayScaleFunction'
tTKContinuousScatterPlot3Display.OSPRayScaleFunction.Points = [0.0, 0.0, 0.5, 0.0, 3.0, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
tTKContinuousScatterPlot3Display.ScaleTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 236649.15403640823, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
tTKContinuousScatterPlot3Display.OpacityTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 236649.15403640823, 1.0, 0.5, 0.0]

# hide data in view
Hide(tetrahedralize2, renderView1)

# Properties modified on tTKContinuousScatterPlot4
tTKContinuousScatterPlot4.ScalarField1 = ['POINTS', 'hole_nto']
tTKContinuousScatterPlot4.ScalarField2 = ['POINTS', 'particle_nto']

# show data in view
tTKContinuousScatterPlot4Display = Show(tTKContinuousScatterPlot4, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
tTKContinuousScatterPlot4Display.Representation = 'Surface'
tTKContinuousScatterPlot4Display.ColorArrayName = [None, '']
tTKContinuousScatterPlot4Display.SelectTCoordArray = 'None'
tTKContinuousScatterPlot4Display.SelectNormalArray = 'None'
tTKContinuousScatterPlot4Display.SelectTangentArray = 'None'
tTKContinuousScatterPlot4Display.OSPRayScaleArray = 'Density'
tTKContinuousScatterPlot4Display.OSPRayScaleFunction = 'PiecewiseFunction'
tTKContinuousScatterPlot4Display.SelectOrientationVectors = 'None'
tTKContinuousScatterPlot4Display.ScaleFactor = 0.0012198532931506634
tTKContinuousScatterPlot4Display.SelectScaleArray = 'Density'
tTKContinuousScatterPlot4Display.GlyphType = 'Arrow'
tTKContinuousScatterPlot4Display.GlyphTableIndexArray = 'Density'
tTKContinuousScatterPlot4Display.GaussianRadius = 6.099266465753317e-05
tTKContinuousScatterPlot4Display.SetScaleArray = ['POINTS', 'Density']
tTKContinuousScatterPlot4Display.ScaleTransferFunction = 'PiecewiseFunction'
tTKContinuousScatterPlot4Display.OpacityArray = ['POINTS', 'Density']
tTKContinuousScatterPlot4Display.OpacityTransferFunction = 'PiecewiseFunction'
tTKContinuousScatterPlot4Display.DataAxesGrid = 'GridAxesRepresentation'
tTKContinuousScatterPlot4Display.PolarAxes = 'PolarAxesRepresentation'
tTKContinuousScatterPlot4Display.ScalarOpacityUnitDistance = 0.00010391120485377063
tTKContinuousScatterPlot4Display.OpacityArrayName = ['POINTS', 'Density']
tTKContinuousScatterPlot4Display.SelectInputVectors = [None, '']
tTKContinuousScatterPlot4Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'OSPRayScaleFunction'
tTKContinuousScatterPlot4Display.OSPRayScaleFunction.Points = [0.0, 0.0, 0.5, 0.0, 3.0, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
tTKContinuousScatterPlot4Display.ScaleTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 65160.03451662292, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
tTKContinuousScatterPlot4Display.OpacityTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 65160.03451662292, 1.0, 0.5, 0.0]

# hide data in view
Hide(tetrahedralize3, renderView1)

# update the view to ensure updated data information
renderView1.Update()

# set active source
SetActiveSource(cuphepheomestate10vti)

# toggle interactive widget visibility (only when running from the GUI)
ShowInteractiveWidgets(proxy=cuphepheomestate10vtiDisplay.SliceFunction)

# toggle interactive widget visibility (only when running from the GUI)
ShowInteractiveWidgets(proxy=cuphepheomestate10vtiDisplay)

# toggle interactive widget visibility (only when running from the GUI)
HideInteractiveWidgets(proxy=cuphepheomestate10vtiDisplay.SliceFunction)

# toggle interactive widget visibility (only when running from the GUI)
HideInteractiveWidgets(proxy=cuphepheomestate10vtiDisplay)

# create a new 'Plane'
plane1 = Plane(registrationName='Plane1')

# Properties modified on plane1
plane1.Origin = [-0.05, -0.05, 0.0]
plane1.Point1 = [0.05, -0.05, 0.0]
plane1.Point2 = [-0.05, 0.05, 0.0]
plane1.XResolution = 1920
plane1.YResolution = 1080

# show data in view
plane1Display = Show(plane1, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
plane1Display.Representation = 'Surface'
plane1Display.ColorArrayName = [None, '']
plane1Display.SelectTCoordArray = 'TextureCoordinates'
plane1Display.SelectNormalArray = 'Normals'
plane1Display.SelectTangentArray = 'None'
plane1Display.OSPRayScaleArray = 'Normals'
plane1Display.OSPRayScaleFunction = 'PiecewiseFunction'
plane1Display.SelectOrientationVectors = 'None'
plane1Display.ScaleFactor = 0.010000000149011612
plane1Display.SelectScaleArray = 'None'
plane1Display.GlyphType = 'Arrow'
plane1Display.GlyphTableIndexArray = 'None'
plane1Display.GaussianRadius = 0.0005000000074505806
plane1Display.SetScaleArray = ['POINTS', 'Normals']
plane1Display.ScaleTransferFunction = 'PiecewiseFunction'
plane1Display.OpacityArray = ['POINTS', 'Normals']
plane1Display.OpacityTransferFunction = 'PiecewiseFunction'
plane1Display.DataAxesGrid = 'GridAxesRepresentation'
plane1Display.PolarAxes = 'PolarAxesRepresentation'
plane1Display.SelectInputVectors = ['POINTS', 'Normals']
plane1Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'OSPRayScaleFunction'
plane1Display.OSPRayScaleFunction.Points = [0.0, 0.0, 0.5, 0.0, 3.0, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
plane1Display.ScaleTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 1.1757813367477812e-38, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
plane1Display.OpacityTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 1.1757813367477812e-38, 1.0, 0.5, 0.0]

# update the view to ensure updated data information
renderView1.Update()

# create a new 'Resample With Dataset'
resampleWithDataset1 = ResampleWithDataset(registrationName='ResampleWithDataset1', SourceDataArrays=tTKContinuousScatterPlot1,
    DestinationMesh=plane1)
resampleWithDataset1.CellLocator = 'Static Cell Locator'

# show data in view
resampleWithDataset1Display = Show(resampleWithDataset1, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
resampleWithDataset1Display.Representation = 'Surface'
resampleWithDataset1Display.ColorArrayName = [None, '']
resampleWithDataset1Display.SelectTCoordArray = 'None'
resampleWithDataset1Display.SelectNormalArray = 'None'
resampleWithDataset1Display.SelectTangentArray = 'None'
resampleWithDataset1Display.OSPRayScaleArray = 'Density'
resampleWithDataset1Display.OSPRayScaleFunction = 'PiecewiseFunction'
resampleWithDataset1Display.SelectOrientationVectors = 'None'
resampleWithDataset1Display.ScaleFactor = 0.010000000149011612
resampleWithDataset1Display.SelectScaleArray = 'Density'
resampleWithDataset1Display.GlyphType = 'Arrow'
resampleWithDataset1Display.GlyphTableIndexArray = 'Density'
resampleWithDataset1Display.GaussianRadius = 0.0005000000074505806
resampleWithDataset1Display.SetScaleArray = ['POINTS', 'Density']
resampleWithDataset1Display.ScaleTransferFunction = 'PiecewiseFunction'
resampleWithDataset1Display.OpacityArray = ['POINTS', 'Density']
resampleWithDataset1Display.OpacityTransferFunction = 'PiecewiseFunction'
resampleWithDataset1Display.DataAxesGrid = 'GridAxesRepresentation'
resampleWithDataset1Display.PolarAxes = 'PolarAxesRepresentation'
resampleWithDataset1Display.SelectInputVectors = [None, '']
resampleWithDataset1Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'OSPRayScaleFunction'
resampleWithDataset1Display.OSPRayScaleFunction.Points = [0.0, 0.0, 0.5, 0.0, 3.0, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
resampleWithDataset1Display.ScaleTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 300725.67137455597, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
resampleWithDataset1Display.OpacityTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 300725.67137455597, 1.0, 0.5, 0.0]

# hide data in view
Hide(plane1, renderView1)

# hide data in view
Hide(tTKContinuousScatterPlot1, renderView1)

# update the view to ensure updated data information
renderView1.Update()

# set active source
SetActiveSource(plane1)

# set active source
SetActiveSource(tTKContinuousScatterPlot2)

# create a new 'Resample With Dataset'
resampleWithDataset2 = ResampleWithDataset(registrationName='ResampleWithDataset2', SourceDataArrays=tTKContinuousScatterPlot2,
    DestinationMesh=plane1)
resampleWithDataset2.CellLocator = 'Static Cell Locator'

# show data in view
resampleWithDataset2Display = Show(resampleWithDataset2, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
resampleWithDataset2Display.Representation = 'Surface'
resampleWithDataset2Display.ColorArrayName = [None, '']
resampleWithDataset2Display.SelectTCoordArray = 'None'
resampleWithDataset2Display.SelectNormalArray = 'None'
resampleWithDataset2Display.SelectTangentArray = 'None'
resampleWithDataset2Display.OSPRayScaleArray = 'Density'
resampleWithDataset2Display.OSPRayScaleFunction = 'PiecewiseFunction'
resampleWithDataset2Display.SelectOrientationVectors = 'None'
resampleWithDataset2Display.ScaleFactor = 0.010000000149011612
resampleWithDataset2Display.SelectScaleArray = 'Density'
resampleWithDataset2Display.GlyphType = 'Arrow'
resampleWithDataset2Display.GlyphTableIndexArray = 'Density'
resampleWithDataset2Display.GaussianRadius = 0.0005000000074505806
resampleWithDataset2Display.SetScaleArray = ['POINTS', 'Density']
resampleWithDataset2Display.ScaleTransferFunction = 'PiecewiseFunction'
resampleWithDataset2Display.OpacityArray = ['POINTS', 'Density']
resampleWithDataset2Display.OpacityTransferFunction = 'PiecewiseFunction'
resampleWithDataset2Display.DataAxesGrid = 'GridAxesRepresentation'
resampleWithDataset2Display.PolarAxes = 'PolarAxesRepresentation'
resampleWithDataset2Display.SelectInputVectors = [None, '']
resampleWithDataset2Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'OSPRayScaleFunction'
resampleWithDataset2Display.OSPRayScaleFunction.Points = [0.0, 0.0, 0.5, 0.0, 3.0, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
resampleWithDataset2Display.ScaleTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 67648.35414950976, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
resampleWithDataset2Display.OpacityTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 67648.35414950976, 1.0, 0.5, 0.0]

# hide data in view
Hide(plane1, renderView1)

# hide data in view
Hide(tTKContinuousScatterPlot2, renderView1)

# update the view to ensure updated data information
renderView1.Update()

# set active source
SetActiveSource(tTKContinuousScatterPlot3)

# create a new 'Resample With Dataset'
resampleWithDataset3 = ResampleWithDataset(registrationName='ResampleWithDataset3', SourceDataArrays=tTKContinuousScatterPlot3,
    DestinationMesh=plane1)
resampleWithDataset3.CellLocator = 'Static Cell Locator'

# show data in view
resampleWithDataset3Display = Show(resampleWithDataset3, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
resampleWithDataset3Display.Representation = 'Surface'
resampleWithDataset3Display.ColorArrayName = [None, '']
resampleWithDataset3Display.SelectTCoordArray = 'None'
resampleWithDataset3Display.SelectNormalArray = 'None'
resampleWithDataset3Display.SelectTangentArray = 'None'
resampleWithDataset3Display.OSPRayScaleArray = 'Density'
resampleWithDataset3Display.OSPRayScaleFunction = 'PiecewiseFunction'
resampleWithDataset3Display.SelectOrientationVectors = 'None'
resampleWithDataset3Display.ScaleFactor = 0.010000000149011612
resampleWithDataset3Display.SelectScaleArray = 'Density'
resampleWithDataset3Display.GlyphType = 'Arrow'
resampleWithDataset3Display.GlyphTableIndexArray = 'Density'
resampleWithDataset3Display.GaussianRadius = 0.0005000000074505806
resampleWithDataset3Display.SetScaleArray = ['POINTS', 'Density']
resampleWithDataset3Display.ScaleTransferFunction = 'PiecewiseFunction'
resampleWithDataset3Display.OpacityArray = ['POINTS', 'Density']
resampleWithDataset3Display.OpacityTransferFunction = 'PiecewiseFunction'
resampleWithDataset3Display.DataAxesGrid = 'GridAxesRepresentation'
resampleWithDataset3Display.PolarAxes = 'PolarAxesRepresentation'
resampleWithDataset3Display.SelectInputVectors = [None, '']
resampleWithDataset3Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'OSPRayScaleFunction'
resampleWithDataset3Display.OSPRayScaleFunction.Points = [0.0, 0.0, 0.5, 0.0, 3.0, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
resampleWithDataset3Display.ScaleTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 233931.71409566255, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
resampleWithDataset3Display.OpacityTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 233931.71409566255, 1.0, 0.5, 0.0]

# hide data in view
Hide(plane1, renderView1)

# hide data in view
Hide(tTKContinuousScatterPlot3, renderView1)

# update the view to ensure updated data information
renderView1.Update()

# set active source
SetActiveSource(tTKContinuousScatterPlot4)

# create a new 'Resample To Line'
resampleToLine1 = ResampleToLine(registrationName='ResampleToLine1', Input=tTKContinuousScatterPlot4)
resampleToLine1.Point1 = [-0.005554244853556156, -0.006199287716299295, 0.0]
resampleToLine1.Point2 = [0.005831829272210598, 0.005999245215207338, 0.0]

# set active source
SetActiveSource(plane1)

# toggle interactive widget visibility (only when running from the GUI)
HideInteractiveWidgets(proxy=resampleToLine1)

# set active source
SetActiveSource(resampleToLine1)

# set active source
SetActiveSource(tTKContinuousScatterPlot4)

# destroy resampleToLine1
Delete(resampleToLine1)
del resampleToLine1

# create a new 'Resample With Dataset'
resampleWithDataset4 = ResampleWithDataset(registrationName='ResampleWithDataset4', SourceDataArrays=tTKContinuousScatterPlot4,
    DestinationMesh=plane1)
resampleWithDataset4.CellLocator = 'Static Cell Locator'

# show data in view
resampleWithDataset4Display = Show(resampleWithDataset4, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
resampleWithDataset4Display.Representation = 'Surface'
resampleWithDataset4Display.ColorArrayName = [None, '']
resampleWithDataset4Display.SelectTCoordArray = 'None'
resampleWithDataset4Display.SelectNormalArray = 'None'
resampleWithDataset4Display.SelectTangentArray = 'None'
resampleWithDataset4Display.OSPRayScaleArray = 'Density'
resampleWithDataset4Display.OSPRayScaleFunction = 'PiecewiseFunction'
resampleWithDataset4Display.SelectOrientationVectors = 'None'
resampleWithDataset4Display.ScaleFactor = 0.010000000149011612
resampleWithDataset4Display.SelectScaleArray = 'Density'
resampleWithDataset4Display.GlyphType = 'Arrow'
resampleWithDataset4Display.GlyphTableIndexArray = 'Density'
resampleWithDataset4Display.GaussianRadius = 0.0005000000074505806
resampleWithDataset4Display.SetScaleArray = ['POINTS', 'Density']
resampleWithDataset4Display.ScaleTransferFunction = 'PiecewiseFunction'
resampleWithDataset4Display.OpacityArray = ['POINTS', 'Density']
resampleWithDataset4Display.OpacityTransferFunction = 'PiecewiseFunction'
resampleWithDataset4Display.DataAxesGrid = 'GridAxesRepresentation'
resampleWithDataset4Display.PolarAxes = 'PolarAxesRepresentation'
resampleWithDataset4Display.SelectInputVectors = [None, '']
resampleWithDataset4Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'OSPRayScaleFunction'
resampleWithDataset4Display.OSPRayScaleFunction.Points = [0.0, 0.0, 0.5, 0.0, 3.0, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
resampleWithDataset4Display.ScaleTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 61472.21960631513, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
resampleWithDataset4Display.OpacityTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 61472.21960631513, 1.0, 0.5, 0.0]

# hide data in view
Hide(plane1, renderView1)

# hide data in view
Hide(tTKContinuousScatterPlot4, renderView1)

# update the view to ensure updated data information
renderView1.Update()

# hide data in view
Hide(resampleWithDataset2, renderView1)

# hide data in view
Hide(resampleWithDataset3, renderView1)

# hide data in view
Hide(resampleWithDataset4, renderView1)

# set scalar coloring
ColorBy(resampleWithDataset4Display, ('POINTS', 'Density'))

# rescale color and/or opacity maps used to include current data range
resampleWithDataset4Display.RescaleTransferFunctionToDataRange(True, False)

# get color transfer function/color map for 'Density'
densityLUT = GetColorTransferFunction('Density')

# get opacity transfer function/opacity map for 'Density'
densityPWF = GetOpacityTransferFunction('Density')

# get 2D transfer function for 'Density'
densityTF2D = GetTransferFunction2D('Density')

# set active source
SetActiveSource(resampleWithDataset1)

# set scalar coloring
ColorBy(resampleWithDataset1Display, ('POINTS', 'Density'))

# rescale color and/or opacity maps used to include current data range
resampleWithDataset1Display.RescaleTransferFunctionToDataRange(True, False)

# show color bar/color legend
resampleWithDataset1Display.SetScalarBarVisibility(renderView1, True)

# hide color bar/color legend
resampleWithDataset1Display.SetScalarBarVisibility(renderView1, False)

# reset view to fit data
renderView1.ResetCamera(False)

# Properties modified on densityLUT
densityLUT.Discretize = 0

# Apply a preset using its name. Note this may not work as expected when presets have duplicate names.
densityLUT.ApplyPreset('Yellow 15', True)

# Rescale transfer function
densityLUT.RescaleTransferFunction(1.0, 300725.67137455597)

# Rescale transfer function
densityPWF.RescaleTransferFunction(1.0, 300725.67137455597)

# convert to log space
densityLUT.MapControlPointsToLogSpace()

# Properties modified on densityLUT
densityLUT.UseLogScale = 1

# Rescale transfer function
densityLUT.RescaleTransferFunction(500.0, 300725.6713745558)

# Rescale transfer function
densityPWF.RescaleTransferFunction(500.0, 300725.6713745558)

# Rescale 2D transfer function
densityTF2D.RescaleTransferFunction(500.0, 300725.6713745558, 0.0, 1.0)

# set active view
SetActiveView(None)

# set active source
SetActiveSource(resampleWithDataset2)

# get active view
renderView2 = GetActiveViewOrCreate('RenderView')

# Create a new 'Render View'
renderView2_1 = CreateView('RenderView')
renderView2_1.AxesGrid = 'GridAxes3DActor'
renderView2_1.StereoType = 'Crystal Eyes'
renderView2_1.CameraFocalDisk = 1.0
renderView2_1.BackEnd = 'OSPRay raycaster'
renderView2_1.OSPRayMaterialLibrary = materialLibrary1

# show data in view
resampleWithDataset2Display_1 = Show(resampleWithDataset2, renderView2_1, 'GeometryRepresentation')

# trace defaults for the display properties.
resampleWithDataset2Display_1.Representation = 'Surface'
resampleWithDataset2Display_1.ColorArrayName = [None, '']
resampleWithDataset2Display_1.SelectTCoordArray = 'None'
resampleWithDataset2Display_1.SelectNormalArray = 'None'
resampleWithDataset2Display_1.SelectTangentArray = 'None'
resampleWithDataset2Display_1.OSPRayScaleArray = 'Density'
resampleWithDataset2Display_1.OSPRayScaleFunction = 'PiecewiseFunction'
resampleWithDataset2Display_1.SelectOrientationVectors = 'None'
resampleWithDataset2Display_1.ScaleFactor = 0.010000000149011612
resampleWithDataset2Display_1.SelectScaleArray = 'Density'
resampleWithDataset2Display_1.GlyphType = 'Arrow'
resampleWithDataset2Display_1.GlyphTableIndexArray = 'Density'
resampleWithDataset2Display_1.GaussianRadius = 0.0005000000074505806
resampleWithDataset2Display_1.SetScaleArray = ['POINTS', 'Density']
resampleWithDataset2Display_1.ScaleTransferFunction = 'PiecewiseFunction'
resampleWithDataset2Display_1.OpacityArray = ['POINTS', 'Density']
resampleWithDataset2Display_1.OpacityTransferFunction = 'PiecewiseFunction'
resampleWithDataset2Display_1.DataAxesGrid = 'GridAxesRepresentation'
resampleWithDataset2Display_1.PolarAxes = 'PolarAxesRepresentation'
resampleWithDataset2Display_1.SelectInputVectors = [None, '']
resampleWithDataset2Display_1.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'OSPRayScaleFunction'
resampleWithDataset2Display_1.OSPRayScaleFunction.Points = [0.0, 0.0, 0.5, 0.0, 3.0, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
resampleWithDataset2Display_1.ScaleTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 67648.35414950976, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
resampleWithDataset2Display_1.OpacityTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 67648.35414950976, 1.0, 0.5, 0.0]

# add view to a layout so it's visible in UI
AssignViewToLayout(view=renderView2_1, layout=layout1, hint=8)

#changing interaction mode based on data extents
renderView2_1.InteractionMode = '2D'
renderView2_1.CameraPosition = [0.0, 0.0, 0.33500000499188903]

# reset view to fit data
renderView2_1.ResetCamera(False)

# set active source
SetActiveSource(resampleWithDataset2)

# set scalar coloring
ColorBy(resampleWithDataset2Display_1, ('POINTS', 'Density'))

# rescale color and/or opacity maps used to include current data range
resampleWithDataset2Display_1.RescaleTransferFunctionToDataRange(True, False)

# show color bar/color legend
resampleWithDataset2Display_1.SetScalarBarVisibility(renderView2_1, True)

# hide color bar/color legend
resampleWithDataset2Display_1.SetScalarBarVisibility(renderView2_1, False)

# Rescale transfer function
densityLUT.RescaleTransferFunction(500.0, 300725.6713745558)

# Rescale transfer function
densityPWF.RescaleTransferFunction(500.0, 300725.6713745558)

# set active view
SetActiveView(None)

# set active source
SetActiveSource(resampleWithDataset3)

# set active source
SetActiveSource(resampleWithDataset3)

# Create a new 'Render View'
renderView3 = CreateView('RenderView')
renderView3.AxesGrid = 'GridAxes3DActor'
renderView3.StereoType = 'Crystal Eyes'
renderView3.CameraFocalDisk = 1.0
renderView3.BackEnd = 'OSPRay raycaster'
renderView3.OSPRayMaterialLibrary = materialLibrary1

# show data in view
resampleWithDataset3Display_1 = Show(resampleWithDataset3, renderView3, 'GeometryRepresentation')

# trace defaults for the display properties.
resampleWithDataset3Display_1.Representation = 'Surface'
resampleWithDataset3Display_1.ColorArrayName = [None, '']
resampleWithDataset3Display_1.SelectTCoordArray = 'None'
resampleWithDataset3Display_1.SelectNormalArray = 'None'
resampleWithDataset3Display_1.SelectTangentArray = 'None'
resampleWithDataset3Display_1.OSPRayScaleArray = 'Density'
resampleWithDataset3Display_1.OSPRayScaleFunction = 'PiecewiseFunction'
resampleWithDataset3Display_1.SelectOrientationVectors = 'None'
resampleWithDataset3Display_1.ScaleFactor = 0.010000000149011612
resampleWithDataset3Display_1.SelectScaleArray = 'Density'
resampleWithDataset3Display_1.GlyphType = 'Arrow'
resampleWithDataset3Display_1.GlyphTableIndexArray = 'Density'
resampleWithDataset3Display_1.GaussianRadius = 0.0005000000074505806
resampleWithDataset3Display_1.SetScaleArray = ['POINTS', 'Density']
resampleWithDataset3Display_1.ScaleTransferFunction = 'PiecewiseFunction'
resampleWithDataset3Display_1.OpacityArray = ['POINTS', 'Density']
resampleWithDataset3Display_1.OpacityTransferFunction = 'PiecewiseFunction'
resampleWithDataset3Display_1.DataAxesGrid = 'GridAxesRepresentation'
resampleWithDataset3Display_1.PolarAxes = 'PolarAxesRepresentation'
resampleWithDataset3Display_1.SelectInputVectors = [None, '']
resampleWithDataset3Display_1.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'OSPRayScaleFunction'
resampleWithDataset3Display_1.OSPRayScaleFunction.Points = [0.0, 0.0, 0.5, 0.0, 3.0, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
resampleWithDataset3Display_1.ScaleTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 233931.71409566255, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
resampleWithDataset3Display_1.OpacityTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 233931.71409566255, 1.0, 0.5, 0.0]

# add view to a layout so it's visible in UI
AssignViewToLayout(view=renderView3, layout=layout1, hint=9)

#changing interaction mode based on data extents
renderView3.InteractionMode = '2D'
renderView3.CameraPosition = [0.0, 0.0, 0.33500000499188903]

# reset view to fit data
renderView3.ResetCamera(False)

# set scalar coloring
ColorBy(resampleWithDataset3Display_1, ('POINTS', 'Density'))

# rescale color and/or opacity maps used to include current data range
resampleWithDataset3Display_1.RescaleTransferFunctionToDataRange(True, False)

# show color bar/color legend
resampleWithDataset3Display_1.SetScalarBarVisibility(renderView3, True)

# Rescale transfer function
densityLUT.RescaleTransferFunction(500.0, 300725.6713745558)

# Rescale transfer function
densityPWF.RescaleTransferFunction(500.0, 300725.6713745558)

# hide color bar/color legend
resampleWithDataset3Display_1.SetScalarBarVisibility(renderView3, False)

# set active view
SetActiveView(None)

# set active source
SetActiveSource(resampleWithDataset4)

# Create a new 'Render View'
renderView4 = CreateView('RenderView')
renderView4.AxesGrid = 'GridAxes3DActor'
renderView4.StereoType = 'Crystal Eyes'
renderView4.CameraFocalDisk = 1.0
renderView4.BackEnd = 'OSPRay raycaster'
renderView4.OSPRayMaterialLibrary = materialLibrary1

# show data in view
resampleWithDataset4Display_1 = Show(resampleWithDataset4, renderView4, 'GeometryRepresentation')

# trace defaults for the display properties.
resampleWithDataset4Display_1.Representation = 'Surface'
resampleWithDataset4Display_1.ColorArrayName = [None, '']
resampleWithDataset4Display_1.SelectTCoordArray = 'None'
resampleWithDataset4Display_1.SelectNormalArray = 'None'
resampleWithDataset4Display_1.SelectTangentArray = 'None'
resampleWithDataset4Display_1.OSPRayScaleArray = 'Density'
resampleWithDataset4Display_1.OSPRayScaleFunction = 'PiecewiseFunction'
resampleWithDataset4Display_1.SelectOrientationVectors = 'None'
resampleWithDataset4Display_1.ScaleFactor = 0.010000000149011612
resampleWithDataset4Display_1.SelectScaleArray = 'Density'
resampleWithDataset4Display_1.GlyphType = 'Arrow'
resampleWithDataset4Display_1.GlyphTableIndexArray = 'Density'
resampleWithDataset4Display_1.GaussianRadius = 0.0005000000074505806
resampleWithDataset4Display_1.SetScaleArray = ['POINTS', 'Density']
resampleWithDataset4Display_1.ScaleTransferFunction = 'PiecewiseFunction'
resampleWithDataset4Display_1.OpacityArray = ['POINTS', 'Density']
resampleWithDataset4Display_1.OpacityTransferFunction = 'PiecewiseFunction'
resampleWithDataset4Display_1.DataAxesGrid = 'GridAxesRepresentation'
resampleWithDataset4Display_1.PolarAxes = 'PolarAxesRepresentation'
resampleWithDataset4Display_1.SelectInputVectors = [None, '']
resampleWithDataset4Display_1.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'OSPRayScaleFunction'
resampleWithDataset4Display_1.OSPRayScaleFunction.Points = [0.0, 0.0, 0.5, 0.0, 3.0, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
resampleWithDataset4Display_1.ScaleTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 61472.21960631513, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
resampleWithDataset4Display_1.OpacityTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 61472.21960631513, 1.0, 0.5, 0.0]

# add view to a layout so it's visible in UI
AssignViewToLayout(view=renderView4, layout=layout1, hint=10)

#changing interaction mode based on data extents
renderView4.InteractionMode = '2D'
renderView4.CameraPosition = [0.0, 0.0, 0.33500000499188903]

# reset view to fit data
renderView4.ResetCamera(False)

# set active source
SetActiveSource(resampleWithDataset4)

# set scalar coloring
ColorBy(resampleWithDataset4Display_1, ('POINTS', 'Density'))

# rescale color and/or opacity maps used to include current data range
resampleWithDataset4Display_1.RescaleTransferFunctionToDataRange(True, False)

# show color bar/color legend
resampleWithDataset4Display_1.SetScalarBarVisibility(renderView4, True)

# Rescale transfer function
densityLUT.RescaleTransferFunction(500.0, 300725.6713745558)

# Rescale transfer function
densityPWF.RescaleTransferFunction(500.0, 300725.6713745558)

# hide color bar/color legend
resampleWithDataset4Display_1.SetScalarBarVisibility(renderView4, False)

# Properties modified on renderView4
renderView4.OrientationAxesVisibility = 0

# set active view
SetActiveView(renderView3)

# Properties modified on renderView3
renderView3.OrientationAxesVisibility = 0

# set active view
SetActiveView(renderView2_1)

# Properties modified on renderView2_1
renderView2_1.OrientationAxesVisibility = 0

# set active view
SetActiveView(renderView1)

# change representation type
resampleWithDataset4Display.SetRepresentationType('Points')

# change representation type
resampleWithDataset4Display.SetRepresentationType('Surface')

# set active source
SetActiveSource(resampleWithDataset1)

# change representation type
resampleWithDataset1Display.SetRepresentationType('Points')

# change representation type
resampleWithDataset1Display.SetRepresentationType('Surface')

# Properties modified on renderView1
renderView1.EnableRayTracing = 0

# Properties modified on renderView1
renderView1.AmbientSamples = 4

# Properties modified on renderView1
renderView1.SamplesPerPixel = 4

# Properties modified on renderView1
renderView1.ProgressivePasses = 4

# set active view
SetActiveView(renderView2_1)

# Properties modified on renderView2_1
renderView2_1.EnableRayTracing = 0

# Properties modified on renderView2_1
renderView2_1.AmbientSamples = 4

# Properties modified on renderView2_1
renderView2_1.SamplesPerPixel = 4

# Properties modified on renderView2_1
renderView2_1.ProgressivePasses = 4

# set active view
SetActiveView(renderView3)

# Properties modified on renderView3
renderView3.EnableRayTracing = 0

# Properties modified on renderView3
renderView3.AmbientSamples = 4

# Properties modified on renderView3
renderView3.SamplesPerPixel = 4

# Properties modified on renderView3
renderView3.ProgressivePasses = 4

# set active view
SetActiveView(renderView4)

# Properties modified on renderView4
renderView4.EnableRayTracing = 0

# Properties modified on renderView4
renderView4.AmbientSamples = 4

# Properties modified on renderView4
renderView4.SamplesPerPixel = 4

# Properties modified on renderView4
renderView4.ProgressivePasses = 4

# set active view
SetActiveView(renderView1)

# layout/tab size in pixels
layout1.SetSize(969, 187)

# current camera placement for renderView1
renderView1.CameraPosition = [0.0, 0.0, 0.4139235195614991]
renderView1.CameraParallelScale = 0.10928014053904922

# save screenshot
SaveScreenshot('../images/cu-PHE-xant-state3-complete.png', renderView1, ImageResolution=[968, 748],
    TransparentBackground=1)

# set active view
SetActiveView(renderView2_1)

# layout/tab size in pixels
layout1.SetSize(969, 187)

# current camera placement for renderView2_1
renderView2_1.InteractionMode = '2D'
renderView2_1.CameraPosition = [0.0, 0.0, 0.41559102257095276]
renderView2_1.CameraParallelScale = 0.1097335851056013

# save screenshot
SaveScreenshot('../images/cu-PHE-xant-state3-cu.png', renderView2_1, ImageResolution=[964, 748],
    TransparentBackground=1)

# set active view
SetActiveView(renderView3)

# layout/tab size in pixels
layout1.SetSize(969, 187)

# current camera placement for renderView3
renderView3.InteractionMode = '2D'
renderView3.CameraPosition = [0.0, 0.0005843857782836856, 0.4139235195614991]
renderView3.CameraFocalPoint = [0.0, 0.0005843857782836856, 0.0]
renderView3.CameraParallelScale = 0.10928014053904922

# save screenshot
SaveScreenshot('../images/cu-PHE-xant-state3-phe.png', renderView3, ImageResolution=[968, 748],
    TransparentBackground=1)

# set active view
SetActiveView(renderView4)

# layout/tab size in pixels
layout1.SetSize(969, 187)

# current camera placement for renderView4
renderView4.InteractionMode = '2D'
renderView4.CameraPosition = [0.0, 0.0, 0.41559102257095276]
renderView4.CameraParallelScale = 0.1097335851056013

# save screenshot
SaveScreenshot('../images/cu-PHE-xant-state3-xant.png', renderView4, ImageResolution=[964, 748],
    TransparentBackground=1)

#================================================================
# addendum: following script captures some of the application
# state to faithfully reproduce the visualization during playback
#================================================================

#--------------------------------
# saving layout sizes for layouts

# layout/tab size in pixels
layout1.SetSize(969, 374)

#-----------------------------------
# saving camera placements for views

# current camera placement for renderView1
renderView1.CameraPosition = [0.0, 0.0, 0.4139235195614991]
renderView1.CameraParallelScale = 0.10928014053904922

# current camera placement for renderView2_1
renderView2_1.InteractionMode = '2D'
renderView2_1.CameraPosition = [0.0, 0.0, 0.41559102257095276]
renderView2_1.CameraParallelScale = 0.1097335851056013

# current camera placement for renderView3
renderView3.InteractionMode = '2D'
renderView3.CameraPosition = [0.0, 0.0005843857782836856, 0.4139235195614991]
renderView3.CameraFocalPoint = [0.0, 0.0005843857782836856, 0.0]
renderView3.CameraParallelScale = 0.10928014053904922

# current camera placement for renderView4
renderView4.InteractionMode = '2D'
renderView4.CameraPosition = [0.0, 0.0, 0.41559102257095276]
renderView4.CameraParallelScale = 0.1097335851056013

#--------------------------------------------
# uncomment the following to render all views
# RenderAllViews()
# alternatively, if you want to write images, you can use SaveScreenshot(...).
